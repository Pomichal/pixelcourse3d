public class HideScreenCommand<T> : ICommand
{
    public void Execute()
    {
        App.screenManager?.Hide<T>();
    }
}
