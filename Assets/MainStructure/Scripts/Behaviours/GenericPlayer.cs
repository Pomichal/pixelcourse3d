using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Main.Behaviours
{
    public class GenericPlayer : MonoBehaviour
    {

        public UnityEvent<bool> onFinish = new UnityEvent<bool>();

        private Rigidbody rigibBody;
        private Quaternion originalRotation;

        protected virtual void Start()
        {
            originalRotation = transform.rotation;
        }

        protected Rigidbody rb
        {
            get
            {
                if(rigibBody == null)
                {
                    rigibBody = GetComponent<Rigidbody>();
                }
                return rigibBody;
            }
        }

        void OnTriggerEnter(Collider other)
        {
            if(other.CompareTag("finish"))
            {
                onFinish.Invoke(true);
            }
        }

        void OnTriggerExit(Collider other)
        {
            if(other.CompareTag("lose"))
            {
                Logger.Log("lose");
                onFinish.Invoke(false);
            }
        }

        public virtual void Reset(Vector3 pos)
        {
            transform.position = pos;
            transform.rotation = originalRotation;
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
    }
}
