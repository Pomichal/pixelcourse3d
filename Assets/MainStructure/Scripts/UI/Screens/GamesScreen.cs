using System.Collections.Generic;
using Commands;

namespace UI
{
    public class GamesScreen : ScreenBase
    {
        public void OpenGameLevels(int gameId)
        {
            new ShowScreenCommand<LevelSelectionScreen>(
                    new Dictionary<string, object>
                    {
                        {"gameId", gameId}
                    }).Execute();
        }
    }
}
