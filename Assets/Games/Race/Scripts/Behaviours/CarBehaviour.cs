using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

namespace Race.Behaviours
{
    [RequireComponent(typeof(Rigidbody))]
    public class CarBehaviour : Main.Behaviours.GenericPlayer
    {
        public List<AxleInfo> axleInfos;
        public float maxMotorTorque;
        public float maxSteeringAngle;
        public float maxBreakForce;

        protected override void Start()
        {
            base.Start();
            foreach(var axleInfo in axleInfos)
            {
                axleInfo.SetOriginalRotations();
            }
        }

        public void FixedUpdate()
        {
            if(App.levelManager.IsStarted)
            {
                float motor = maxMotorTorque * Input.GetAxis("Vertical");
                float steering = maxSteeringAngle * Input.GetAxis("Horizontal");
                float breaking = Input.GetButton("Jump") ? maxBreakForce : 0;

                foreach (AxleInfo axleInfo in axleInfos) {
                    if (axleInfo.steering) {
                        axleInfo.leftWheelColl.steerAngle = steering;
                        axleInfo.rightWheelColl.steerAngle = steering;
                    }
                    if (axleInfo.motor) {
                        axleInfo.leftWheelColl.motorTorque = motor;
                        axleInfo.rightWheelColl.motorTorque = motor;
                    }
                    axleInfo.leftWheelColl.brakeTorque = breaking;
                    axleInfo.rightWheelColl.brakeTorque = breaking;
                    axleInfo.UpdateVisuals();
                }
            }
        }

        public override void Reset(Vector3 pos)
        {
            base.Reset(pos);
        }
    }
}
