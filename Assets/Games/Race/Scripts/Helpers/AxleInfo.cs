using System;
using System.Collections.Generic;
using UnityEngine;

namespace Race.Behaviours
{
    [System.Serializable]
    public class AxleInfo
    {
        public WheelCollider leftWheelColl;
        public Transform leftWheelMesh;
        public WheelCollider rightWheelColl;
        public Transform rightWheelMesh;
        public bool motor;
        public bool steering;

        private Quaternion leftOriginal;
        private Quaternion rightOriginal;

        public void SetOriginalRotations()
        {
            leftOriginal = leftWheelMesh.rotation;
            rightOriginal = rightWheelMesh.rotation;
        }

        public void UpdateVisuals()
        {
            ChangeVisual(leftWheelColl, leftWheelMesh, leftOriginal);
            ChangeVisual(rightWheelColl, rightWheelMesh, rightOriginal);
        }

        private void ChangeVisual(WheelCollider coll, Transform visual, Quaternion origin)
        {
            Vector3 pos;
            Quaternion rot;
            coll.GetWorldPose(out pos, out rot);
            visual.position = pos;
            visual.rotation = rot * origin;
        }
    }
}
