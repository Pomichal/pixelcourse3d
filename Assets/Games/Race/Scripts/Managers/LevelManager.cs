using System.Collections;
using System.Collections.Generic;
using Commands;
using Race.Behaviours;
using UI;
using UnityEngine;

namespace Race.Managers
{
    public class LevelManager : Main.Managers.LevelManager
    {

        public CarBehaviour car;
        public List<GameObject> cameras;
        public int activeCameraId;
        public int particleCameraId;
        public ParticleSystem winParticles;

        private int firstCameraId;

        protected override void FirstTimeSetup()
        {
            firstCameraId = activeCameraId;
            if(cameras != null && cameras.Count > 0)
            {
                foreach(var camObject in cameras)
                {
                    camObject.SetActive(false);
                }
                cameras[activeCameraId].SetActive(true);
            }
            player = car;
            base.FirstTimeSetup();
        }

        public override void SetupScene()
        {
            base.SetupScene();
            SetActiveCamera(firstCameraId);
            if(winParticles != null)
            {
                winParticles.Stop();
            }
            new ShowScreenCommand<CountdownInGameScreen>().Execute();
        }

        protected override void Update()
        {
            base.Update();
            if(cameras != null && Input.GetKeyDown(KeyCode.C))
            {
                SetActiveCamera((activeCameraId + 1) % cameras.Count);
            }

        }

        public override void Finish(bool win)
        {
            base.Finish(win);
            if(winParticles != null)
            {
                winParticles.Play();
            }
            SetActiveCamera(particleCameraId);
            new HideScreenCommand<CountdownInGameScreen>().Execute();
            var par = new Dictionary<string, object>
            {
                {"win", win},
                {"time", Time.time - timeSinceStart}
            };
            new ShowScreenCommand<GameOverScreen>(par).Execute();
        }

        public void SetActiveCamera(int cameraId)
        {
            if(cameras == null || cameras.Count == 0 && cameras.Count >= cameraId) return;
            cameras[activeCameraId].SetActive(false);
            activeCameraId = cameraId;
            cameras[activeCameraId].SetActive(true);
        }
    }
}
