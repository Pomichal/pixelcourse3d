using System.Collections.Generic;
using Commands;
using Maze.Behaviours;
using UI;
using UnityEngine;

namespace Maze.Managers
{
    public class LevelManager : Main.Managers.LevelManager
    {

        public PlayerBehaviour playerPrefab;
        public List<Rigidbody> floors;

        protected override void FirstTimeSetup()
        {
            player = Instantiate<PlayerBehaviour>(playerPrefab, startPosition);
            base.FirstTimeSetup();
        }

        public override void Finish(bool win)
        {
            base.Finish(win);
            new HideScreenCommand<CountdownInGameScreen>().Execute();
            var par = new Dictionary<string, object>
            {
                {"win", win},
                {"time", Time.time - timeSinceStart}
            };
            new ShowScreenCommand<GameOverScreen>(par).Execute();
        }

        public override void SetupScene()
        {
            base.SetupScene();
            new ShowScreenCommand<CountdownInGameScreen>().Execute();
            foreach(Rigidbody floor in floors)
            {
                floor.angularVelocity = Vector3.zero;
                floor.velocity = Vector3.zero;
                floor.transform.rotation = Quaternion.identity;
            }
        }
    }
}
