using UnityEngine;

namespace Maze.Behaviours
{
    [RequireComponent(typeof(Rigidbody))]
    public class MazeBehaviour : MonoBehaviour
    {

        public float rotationSpeed;

        private Rigidbody rb;

        void Awake()
        {
            rb = GetComponent<Rigidbody>();
        }

        void Update()
        {
            float ver = Input.GetAxis("Vertical") * Time.deltaTime * rotationSpeed;
            float hor = Input.GetAxis("Horizontal") * Time.deltaTime * rotationSpeed;
            if(ver != 0 || hor != 0)
            {
                rb.AddTorque(ver, 0, hor, ForceMode.VelocityChange);
            }
        }
    }
}
